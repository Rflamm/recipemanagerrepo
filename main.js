const {
  app,
  BrowserWindow
} = require('electron')
// RFF This is to live reload, but it seems that I first have to fix the refresh issue
require('electron-reload')(__dirname, {
	electron: require(`${__dirname}/node_modules/electron`)
});
const url = require("url");
const path = require("path");
// did-fail-load
let appWindow

function initWindow() {
  appWindow = new BrowserWindow({
    width: 1200,
    height: 1000,
    webPreferences: {
      nodeIntegration: true
    }
  })

  //Used to reload the app
  function reloadWindow() {
    appWindow.reload();
    appWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, `/dist/index.html`),
        protocol: "file:",
        slashes: true
      })
    )
  }

  //Even logger
  function logEvent(eventName) {
    appWindow.webContents.on(eventName, function() {
      console.log(eventName, [].slice.call(arguments, 1));
    });
  }

  //logging
  console.log("URL is: ", path.join(__dirname, `/dist/index.html`));
  logEvent('did-start-loading');
  logEvent('did-fail-provisional-load');
  logEvent('did-fail-load');
  logEvent('did-finish-load');

  // Electron Build Path
  appWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, `/dist/index.html`),
      protocol: "file:",
      slashes: true
    })
  );

  //Work around to get app to reload
  appWindow.webContents.on('did-fail-load', () => {
    console.log('did-fail-load');
    reloadWindow();
  });


  // Initialize the DevTools.
  //appWindow.webContents.openDevTools()

  appWindow.on('closed', function () {
    appWindow = null
  })
}

app.on('ready', initWindow)

// Close when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


app.on('activate', function () {
  if (win === null) {
    initWindow()
  }
})



// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

